//
//  ListViewController.swift
//  Sign Up Page
//
//  Created by apple on 27/03/15.
//  Copyright (c) 2015 Bhavishya. All rights reserved.
//

import UIKit

class ListViewController: UIViewController, UITableViewDelegate, UISearchBarDelegate, UITableViewDataSource, UISearchDisplayDelegate {
    //taken these elements just to test...
    var dataArray =  ["Apple", "Samsung", "iPhone", "iPad", "Macbook", "iMac" , "Mac Mini"]
    var is_searching:Bool!
    var searchingDataArray:NSMutableArray! // Its data searching array that need for search result show
    
    @IBOutlet weak var searchBarObj: UISearchBar!
    @IBOutlet weak var tableViews: UITableView!
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if is_searching == true{
            return searchingDataArray.count
        } else {
            return dataArray.count  //Currently Giving default Value
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:UITableViewCell = self.tableViews.dequeueReusableCellWithIdentifier("cell") as UITableViewCell
        if is_searching == true{
            cell.textLabel?.text = searchingDataArray[indexPath.row] as NSString
        }else{
            cell.textLabel?.text = dataArray[indexPath.row] as NSString
        }
        return cell
    }
    
    //working of search bar...
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String){
        if searchBar.text.isEmpty{
            is_searching = false
            tableViews.reloadData()
        } else {
            println(" search text %@ ",searchBar.text as NSString)
            is_searching = true
            searchingDataArray.removeAllObjects()
            for var index = 0; index < dataArray.count; index++
            {
                var currentString = dataArray[index] as String
                if currentString.lowercaseString.rangeOfString(searchText.lowercaseString)  != nil {
                    searchingDataArray.addObject(currentString)
                }
            }
            tableViews.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        is_searching = false
        searchingDataArray = []
        //registering class...
        self.tableViews.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

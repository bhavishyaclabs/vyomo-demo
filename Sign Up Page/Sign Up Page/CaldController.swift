//
//  CaldController.swift
//  Sign Up Page
//
//  Created by apple on 02/04/15.
//  Copyright (c) 2015 Bhavishya. All rights reserved.
//

import UIKit

class CaldController: UIViewController, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UITableViewDelegate {

    
   
    @IBOutlet var secondTableList: UITableView!
    @IBOutlet var tableList: UITableView!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var scrollDatesWeek: UIScrollView!
    @IBOutlet var dayView: UIView!
    @IBOutlet var weekView: UIView!
    @IBOutlet var monthView: UIView!
    
    
    @IBAction func dayButton(sender: AnyObject) {
        secondTableList.hidden = false
        dayView.hidden = false
        tableList.hidden = false
    }
    @IBAction func weekButton(sender: AnyObject) {
        weekView.hidden = false
        tableList.hidden = false
    }
    @IBAction func monthButton(sender: AnyObject) {
        monthView.hidden = false
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
    
      let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 20, left: 10, bottom: 10, right: 10)
        layout.itemSize = CGSize(width: 90, height: 90)
    
      //  collectionView!.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: "CollectionViewCell")
        tableList!.registerClass(CustomTableViewCell.self, forCellReuseIdentifier: "TableViewCell")
    }

    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 42
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cellss = collectionView.dequeueReusableCellWithReuseIdentifier("CollectionViewCell", forIndexPath: indexPath) as UICollectionViewCell
        
        return cellss
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) ->   UITableViewCell {
        //to remove seperators...
        
        let celll = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "TableViewCell")
        return celll
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 20
    }
}

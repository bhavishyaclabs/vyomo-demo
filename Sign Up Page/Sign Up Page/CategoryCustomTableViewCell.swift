//
//  CategoryCustomTableViewCell.swift
//  Sign Up Page
//
//  Created by apple on 02/04/15.
//  Copyright (c) 2015 Bhavishya. All rights reserved.
//

import UIKit

class CategoryCustomTableViewCell: UITableViewCell {

    var status: Bool = false
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    let images = UIImage(named: "unselecteded_icon_1x.png") as UIImage?
    let buttons: UIButton   = UIButton()
    buttons.frame = CGRectMake(350, 10, 30, 30)
    buttons.setImage(images, forState: .Normal)
    buttons.addTarget(self, action: "Act", forControlEvents:.TouchUpInside)
    contentView.addSubview(buttons)
    }
    func Act() {
        if status == false {
        let images = UIImage(named: "selected_icon_2x.png") as UIImage?
        let buttons: UIButton   = UIButton()
        buttons.frame = CGRectMake(350, 10, 30, 30)
        buttons.setImage(images, forState: .Normal)
        buttons.tintColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
        contentView.addSubview(buttons)
        status = true
        } else {
            let images = UIImage(named: "unselecteded_icon_1x.png") as UIImage?
            let buttons: UIButton   = UIButton()
            buttons.frame = CGRectMake(350, 10, 30, 30)
            buttons.setImage(images, forState: .Normal)
            buttons.tintColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
            contentView.addSubview(buttons)
            status = false
        }
    }
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

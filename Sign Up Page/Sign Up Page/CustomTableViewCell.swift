//
//  CustomTableViewCell.swift
//  Sign Up Page
//
//  Created by apple on 31/03/15.
//  Copyright (c) 2015 Bhavishya. All rights reserved.
//

import UIKit

class CustomTableViewCell: UITableViewCell {
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        var cellView = UIView(frame: CGRectMake(0, 10,100, 100))
        cellView.backgroundColor = UIColor.clearColor()
        contentView.addSubview(cellView)
        
        //Views used...
        var view2: UIView = UIView()
        view2.frame = CGRectMake(80/3, 280/3, 1000/3, 1)
        view2.backgroundColor = UIColor.grayColor()
        view2.tag = 10
        view2.translatesAutoresizingMaskIntoConstraints() 
        contentView.addSubview(view2)
        
        //Labels used...
        var Label1 = UILabel(frame: CGRectMake(80/3, 50/3, 100, 30))
        Label1.text = ""
        Label1.tag = 11
        contentView.addSubview(Label1)
        
        //textFields used...
        var txtField: UITextField = UITextField()
        txtField.frame = CGRectMake(80/3, 160/3, 300, 30)
        txtField.placeholder = "Enter your username"
        txtField.backgroundColor = UIColor.whiteColor()
        txtField.tag = 12
        contentView.addSubview(txtField)
        
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}

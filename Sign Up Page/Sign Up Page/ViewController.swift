//
//  ViewController.swift
//  Sign Up Page
//
//  Created by apple on 25/03/15.
//  Copyright (c) 2015 Bhavishya. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITextFieldDelegate {
    @IBOutlet weak var tasksTable: UITableView! //table view
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // setting row height...
        self.tasksTable.rowHeight = 100
        //registering custom class...
        self.tasksTable.registerClass(CustomTableViewCell.self, forCellReuseIdentifier: "CellTable")
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 13
    }
    // MARK: - UITextFieldDelegate
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        return true;
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        var cell2: UITableViewCell = textField.superview as UITableViewCell
        var table: UITableView = cell2.superview as UITableView
        let textFieldIndexPath = table.indexPathForCell(cell2)
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) ->   UITableViewCell {
        //to remove seperators...
        tasksTable.separatorStyle = .None
        let cell = CustomTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "CellTable")
        if indexPath.row == 0 {
            //for cell not to b selected on click
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            //Calling text field from custom class...
            let TextField: UITextField = cell.viewWithTag(12) as UITextField
            TextField.removeFromSuperview()
            //making image...
            var menuImage = UIImageView(frame: CGRectMake(100, 150/3, 200, 50))
            menuImage.backgroundColor = UIColor.clearColor()
            menuImage.image = UIImage(named: "vyomo-logo-hd + Business_3x.png")
            cell.addSubview(menuImage)
            //Making line thorugh UIView...
            let viewSeperator: UIView = cell.viewWithTag(10) as UIView!
            viewSeperator.backgroundColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
            cell.backgroundColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
        }
        
        if indexPath.row == 1 {
            //for cell not to b selected on click
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            //setting up of label...
            let LabelField: UILabel = cell.viewWithTag(11) as UILabel
            LabelField.text = "Sign Up"
            LabelField.frame = CGRectMake(150, 20, 100, 30)
            LabelField.textAlignment = NSTextAlignment.Center
            LabelField.textColor = UIColor.whiteColor()
            //removing text field...
            let TextField: UITextField = cell.viewWithTag(12) as UITextField
            TextField.removeFromSuperview()
            //changing color of UIView line...
            let made: UIView = cell.viewWithTag(10) as UIView!
            made.backgroundColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
            //setting up of button...
            var button = UIButton.buttonWithType(UIButtonType.System) as UIButton
            button.frame = CGRectMake(100, 140/3, 200, 30)
            button.backgroundColor = UIColor.clearColor()
            button.setTitle("Not a stylish? click here", forState: UIControlState.Normal)
            button.addTarget(self, action: "Action1", forControlEvents: UIControlEvents.TouchUpInside)
            button.setTitleColor(UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1), forState: .Normal)
            cell.addSubview(button)
            
            cell.backgroundColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
        }
        
        if indexPath.row == 2{
            //setting up of label...
            let LabelField: UILabel = cell.viewWithTag(11) as UILabel
            LabelField.text = "Username"
        }
        
        if indexPath.row == 3 {
            //setting up of label...
            let LabelField: UILabel = cell.viewWithTag(11) as UILabel
            LabelField.text = "First Name"
        }
        
        if indexPath.row == 4 {
            //setting up of label...
            let LabelField: UILabel = cell.viewWithTag(11) as UILabel
            LabelField.text = "Last Name"
        }
        
        if indexPath.row == 5 {
            //setting up of label...
            let LabelField: UILabel = cell.viewWithTag(11) as UILabel
            LabelField.text = "Phone No"
            //Calling text field from custom class...
            let textField: UITextField = cell.viewWithTag(12) as UITextField
            textField.placeholder = "Phone no"
            textField.frame = CGRectMake(120, 40, 300, 30)
            //setting image...
            var menuImage = UIImageView(frame: CGRectMake(200/3, 50, 15, 12))
            menuImage.backgroundColor = UIColor.clearColor()
            menuImage.image = UIImage(named: "dropdown_icon_3x.png")
            cell.addSubview(menuImage)
            //setting UIView...
            var view2: UIView = UIView()
            view2.frame = CGRectMake(100, 45, 1, 20)
            view2.backgroundColor = UIColor.grayColor()
            view2.tag = 10
            cell.addSubview(view2)
        }
        
        if indexPath.row == 6 {
            //setting up of label...
            let LabelField: UILabel = cell.viewWithTag(11) as UILabel
            LabelField.text = "Email"
        }
        
        if indexPath.row == 7 {
            //setting up of label...
            let LabelField: UILabel = cell.viewWithTag(11) as UILabel
            LabelField.text = "Password"
        }
        
        if indexPath.row == 8 {
            //setting up of label...
            let LabelField: UILabel = cell.viewWithTag(11) as UILabel
            LabelField.text = "Confirm Password"
            LabelField.frame = CGRect(x: 80/3, y: 50/3, width: 200, height: 30)
        }
        
        if indexPath.row == 9 {
            //setting up of label...
            let LabelField: UILabel = cell.viewWithTag(11) as UILabel
            LabelField.text = "I am"
            //setting up of button...
            var button   = UIButton.buttonWithType(UIButtonType.System) as UIButton
            button.frame = CGRectMake(840/3, 90/3, 70, 20)
            button.backgroundColor = UIColor.clearColor()
            button.setTitle("Manager", forState: UIControlState.Normal)
            button.addTarget(self, action: "Action1", forControlEvents: UIControlEvents.TouchUpInside)
            button.setTitleColor(UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1), forState: .Normal)
            cell.addSubview(button)
            //Calling text field from custom class...
            let TextField: UITextField = cell.viewWithTag(12) as UITextField
            TextField.removeFromSuperview()
            //setting image...
            let image = UIImage(named: "small_right_arrow_icon_3x.png") as UIImage?
            let button2   = UIButton.buttonWithType(UIButtonType.System) as UIButton
            button2.frame = CGRectMake(350, 90/3, 20, 20)
            button2.setImage(image, forState: .Normal)
            button2.tintColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
            button2.addTarget(self, action: "Action1", forControlEvents:.TouchUpInside)
            cell.addSubview(button2)
            
        }
        
        if indexPath.row == 10 {
            //setting up of label...
            let LabelField: UILabel = cell.viewWithTag(11) as UILabel
            LabelField.text = "Salon Name"
            //This button gets u to a new screen...
            var button   = UIButton.buttonWithType(UIButtonType.System) as UIButton
            button.frame = CGRectMake(280, 90/3, 70, 20)
            button.backgroundColor = UIColor.whiteColor()
            button.setTitle("Add New", forState: UIControlState.Normal)
            button.addTarget(self, action: "Action2", forControlEvents: UIControlEvents.TouchUpInside)
            button.setTitleColor(UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1), forState: .Normal)
            cell.addSubview(button)
            //Calling text field from custom class...
            let TextField: UITextField = cell.viewWithTag(12) as UITextField
            TextField.removeFromSuperview()
            //setting image...
            let image = UIImage(named: "small_right_arrow_icon_3x.png") as UIImage?
            let button2   = UIButton.buttonWithType(UIButtonType.System) as UIButton
            button2.frame = CGRectMake(350, 90/3, 20, 20)
            button2.setImage(image, forState: .Normal)
            button2.tintColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
            button2.addTarget(self, action: "Action2", forControlEvents:.TouchUpInside)
            cell.addSubview(button2)
            
        }
        
        if indexPath.row == 11 {
            //setting up of label...
            let LabelField: UILabel = cell.viewWithTag(11) as UILabel
            LabelField.text = "Phone No"
            //Calling text field from custom class...
            let textField: UITextField = cell.viewWithTag(12) as UITextField
            textField.placeholder = "Enter phone no of sales promoters"
        }
        
        if indexPath.row == 12 {
            //setting up of label...
            let LabelField: UILabel = cell.viewWithTag(11) as UILabel
            LabelField.frame =  CGRectMake(150/3, 20, 250, 30)
            LabelField.text = "I agree to Terms of Service, Privacy Policy. EULA"
            LabelField.font = UIFont.systemFontOfSize(12)
            LabelField.lineBreakMode = .ByWordWrapping // or NSLineBreakMode.ByWordWrapping
            LabelField.numberOfLines = 2
            let TextField: UITextField = cell.viewWithTag(12) as UITextField
            TextField.removeFromSuperview()
            //setting up of button...
            let image = UIImage(named: "Unchecked_box_icon_3x.png") as UIImage?
            let button2   = UIButton.buttonWithType(UIButtonType.System) as UIButton
            button2.frame = CGRectMake(80/3, 30, 10, 10)
            button2.setImage(image, forState: .Normal)
            button2.tintColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
            button2.tag = 9
            button2.addTarget(self, action: "Action3", forControlEvents:.TouchUpInside)
            cell.addSubview(button2)
            
        }
        return cell
    }
    //functions used when button pressed...
    func Action3() {
        //can change button to checked...
    }
    func Action2() {
        self.performSegueWithIdentifier("newscreen", sender: self)
    }
    func Action1() {
        //self.performSegueWithIdentifier("newscreen", sender: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) { //hides keyboard if clicked on empty space.
        self.view.endEditing(true)
        
    }
}





//
//  CategoriesViewController.swift
//  Sign Up Page
//
//  Created by apple on 30/03/15.
//  Copyright (c) 2015 Bhavishya. All rights reserved.
//

import UIKit

class CategoriesViewController: UIViewController, UITableViewDelegate {
    //Array for example...
    let swiftBlogs = ["Hair Cut", "Wax", "Spa", "Hair Color", "Massage", "Facial", "Threading", "Hair massage", "Nail art", "Conditioning"]
    
    @IBOutlet weak var labelCatogories: UILabel!
    @IBOutlet weak var listTable: UITableView!
    var view2: UIView = UIView()
    
    //Does nothing...
    @IBAction func continueButton(sender: AnyObject) {
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return swiftBlogs.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cel = CategoryCustomTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "Sell")
        cel.textLabel?.text = swiftBlogs[indexPath.row]
        return cel
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

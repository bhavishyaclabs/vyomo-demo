//
//  InformationViewController.swift
//  Sign Up Page
//
//  Created by apple on 31/03/15.
//  Copyright (c) 2015 Bhavishya. All rights reserved.
//

import UIKit

class InformationViewController: UIViewController, UITableViewDelegate {
    
    @IBOutlet weak var tblView: UITableView!
    
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if section == 0 {
            return 6
        } else {
            return 5        }
    }
    
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        let headerCellular = InformationTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "HeaderCell")
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                //for cell not to b selected on click
                headerCellular.selectionStyle = UITableViewCellSelectionStyle.None
                //adding label...
                let LabelField: UILabel = headerCellular.viewWithTag(15) as UILabel
                LabelField.text = "Add Salon Info"
                LabelField.frame = CGRectMake(80, 100/3, 250, 50)
                LabelField.font = UIFont.boldSystemFontOfSize(20.0)
                LabelField.textAlignment = NSTextAlignment.Center
                LabelField.textColor = UIColor.whiteColor()
                
                //adding image...
                let image = UIImage(named: "left_arrow_3x.png") as UIImage?
                let button2   = UIButton.buttonWithType(UIButtonType.System) as UIButton
                button2.frame = CGRectMake(10, 25, 50, 50)
                button2.backgroundColor = UIColor.clearColor()
                button2.setImage(image, forState: .Normal)
                button2.tintColor = UIColor.whiteColor()
                button2.addTarget(self, action: "Action", forControlEvents:.TouchUpInside)
                headerCellular.addSubview(button2)
                headerCellular.backgroundColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
                //Setting row height dynamically...
                tblView.rowHeight = 100
                headerCellular.frame.size.height = 100
            }
            
            if indexPath.row == 1 {
                let LabelField: UILabel = headerCellular.viewWithTag(15) as UILabel
                LabelField.text = "Role"
                let Label2 = UILabel(frame: CGRectMake(80/3, 160/3, 100, 30))
                Label2.text = "Manager"
                Label2.textColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
                headerCellular.addSubview(Label2)
                //Setting row height dynamically...
                tblView.rowHeight = 100
                headerCellular.frame.size.height = 100
            }
            if indexPath.row == 2 {
                let LabelField: UILabel = headerCellular.viewWithTag(15) as UILabel
                LabelField.text = "Salon Name"
                
                //textFields used...
                var txtField: UITextField = UITextField()
                txtField.frame = CGRectMake(80/3, 160/3, 300, 30)
                txtField.placeholder = "Enter Salon Name"
                txtField.backgroundColor = UIColor.whiteColor()
                headerCellular.addSubview(txtField)
                //Setting row height dynamically...
                tblView.rowHeight = 100
                headerCellular.frame.size.height = 100
            }
            if indexPath.row == 3 {
                let LabelField: UILabel = headerCellular.viewWithTag(15) as UILabel
                LabelField.text = "Address"
                
                //textFields used...
                var txtField: UITextField = UITextField()
                txtField.frame = CGRectMake(80/3, 160/3, 300, 30)
                txtField.placeholder = "Pin Your Address"
                txtField.backgroundColor = UIColor.whiteColor()
                headerCellular.addSubview(txtField)
                //Setting row height dynamically...
                tblView.rowHeight = 100
                headerCellular.frame.size.height = 100
            }
            if indexPath.row == 4 {
                let LabelField: UILabel = headerCellular.viewWithTag(15) as UILabel
                LabelField.text = "Phone No"
                //seting image...
                var menuImage = UIImageView(frame: CGRectMake(200/3, 50, 15, 12))
                menuImage.backgroundColor = UIColor.clearColor()
                menuImage.image = UIImage(named: "dropdown_icon_3x.png")
                menuImage.tintColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
                headerCellular.addSubview(menuImage)
                
                var view2: UIView = UIView()
                view2.frame = CGRectMake(100, 45, 1, 20)
                view2.backgroundColor = UIColor.grayColor()
                view2.tag = 10
                headerCellular.addSubview(view2)
                
                //textFields used...
                var txtFields: UITextField = UITextField()
                txtFields.frame = CGRectMake(120, 40, 300, 30)
                txtFields.placeholder = "Phone No"
                txtFields.backgroundColor = UIColor.whiteColor()
                headerCellular.addSubview(txtFields)
                //Setting row height dynamically...
                tblView.rowHeight = 100
                headerCellular.frame.size.height = 100
            }
            if indexPath.row == 5 {
                let LabelField: UILabel = headerCellular.viewWithTag(15) as UILabel
                LabelField.frame = CGRectMake(80/3, 70/3, 100, 30)
                LabelField.text = "Salon Name"
                //Setting row height dynamically...
                tblView.rowHeight = 70
                headerCellular.frame.size.height = 70
                //setting button...
                var button   = UIButton.buttonWithType(UIButtonType.System) as UIButton
                button.frame = CGRectMake(840/3, 70/3, 70, 20)
                button.backgroundColor = UIColor.clearColor()
                button.setTitle("Select", forState: UIControlState.Normal)
                button.addTarget(self, action: "Action1", forControlEvents: UIControlEvents.TouchUpInside)
                button.setTitleColor(UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1), forState: .Normal)
                headerCellular.addSubview(button)
                
                //seting image...
                let image = UIImage(named: "small_right_arrow_icon_3x.png") as UIImage?
                let button2   = UIButton.buttonWithType(UIButtonType.System) as UIButton
                button2.frame = CGRectMake(350, 70/3, 20, 20)
                button2.setImage(image, forState: .Normal)
                button2.tintColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
                button2.addTarget(self, action: "Action1", forControlEvents:.TouchUpInside)
                headerCellular.addSubview(button2)
                
            }
            
        } else if indexPath.section == 1 {
            if indexPath.row == 0 {
                
                let LabelField: UILabel = headerCellular.viewWithTag(15) as UILabel
                LabelField.text = "Front View"
                //seting image...
                let image = UIImage(named: "small_right_arrow_icon_3x.png") as UIImage?
                let button2   = UIButton.buttonWithType(UIButtonType.System) as UIButton
                button2.frame = CGRectMake(350, 100/3, 20, 20)
                button2.setImage(image, forState: .Normal)
                button2.tintColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
                button2.addTarget(self, action: "Action1", forControlEvents:.TouchUpInside)
                headerCellular.addSubview(button2)
                //Setting row height dynamically...
                tblView.rowHeight = 70
                headerCellular.frame.size.height = 70
            }
            
            if indexPath.row == 1 {
                
                let LabelField: UILabel = headerCellular.viewWithTag(15) as UILabel
                LabelField.text = "Inside View"
                //setting image...
                let image = UIImage(named: "small_right_arrow_icon_3x.png") as UIImage?
                let button2   = UIButton.buttonWithType(UIButtonType.System) as UIButton
                button2.frame = CGRectMake(350, 100/3, 20, 20)
                button2.setImage(image, forState: .Normal)
                button2.tintColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
                button2.addTarget(self, action: "Action1", forControlEvents:.TouchUpInside)
                headerCellular.addSubview(button2)
                //Setting row height dynamically...
                tblView.rowHeight = 70
                headerCellular.frame.size.height = 70
            }
            if indexPath.row == 2 {
                
                let LabelField: UILabel = headerCellular.viewWithTag(15) as UILabel
                LabelField.text = "Stylist Photo"
                //seting image...
                let image = UIImage(named: "small_right_arrow_icon_3x.png") as UIImage?
                let button2   = UIButton.buttonWithType(UIButtonType.System) as UIButton
                button2.frame = CGRectMake(350, 100/3, 20, 20)
                button2.setImage(image, forState: .Normal)
                button2.tintColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
                button2.addTarget(self, action: "Action1", forControlEvents:.TouchUpInside)
                headerCellular.addSubview(button2)
                //Setting row height dynamically...
                tblView.rowHeight = 70
                headerCellular.frame.size.height = 70
            }
            if indexPath.row == 3 {
                
                let LabelField: UILabel = headerCellular.viewWithTag(15) as UILabel
                LabelField.text = "Description"
                //Text view taken for multiple lines input...
                var textView = UITextView(frame: CGRect(x: 80/3, y: 150/3, width: 350, height: 100))
                headerCellular.addSubview(textView)
                //Setting row height dynamically...
                tblView.rowHeight = 150
                headerCellular.frame.size.height = 150
                
                
            }
            if indexPath.row == 4 {
                var button   = UIButton.buttonWithType(UIButtonType.System) as UIButton
                button.frame = CGRectMake(0, 0, 400,70)
                button.backgroundColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
                button.setTitle("Save", forState: UIControlState.Normal)
                button.addTarget(self, action: "Action3", forControlEvents: UIControlEvents.TouchUpInside)
                button.setTitleColor(UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1), forState: .Normal)
                headerCellular.addSubview(button)
                //Setting row height dynamically...
                tblView.rowHeight = 70
                headerCellular.frame.size.height = 70
            }
        }
        
        return headerCellular
    }
    //function called on button click...
    func Action3() {
        self.performSegueWithIdentifier("back", sender: self)
    }
    func Action () {
        self.performSegueWithIdentifier("backAction", sender: self)
    }
    func Action1() {
        
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        if section == 0 {
            return 1.0
        } else {
            return 20.0
        }
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let  headerCell = tableView.dequeueReusableCellWithIdentifier("HeaderCell") as UITableViewCell
        var headerLabel:UILabel = UILabel()
        headerCell.contentView.addSubview(headerLabel)
        switch(section) {
        case 0:
            headerCell.textLabel?.text = ""
            headerCell.backgroundColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
            
        case 1:
            headerCell.textLabel?.text = "Upload Salon Photos"
            headerCell.backgroundColor = UIColor.lightGrayColor()
            
        default:
            headerCell.removeFromSuperview()
            
        }
        return headerCell
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //registering custom class...
        self.tblView.registerClass(InformationTableViewCell.self, forCellReuseIdentifier: "CellTable")
        self.tblView.autoresizesSubviews = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

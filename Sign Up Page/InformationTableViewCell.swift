//
//  InformationTableViewCell.swift
//  Sign Up Page
//
//  Created by apple on 31/03/15.
//  Copyright (c) 2015 Bhavishya. All rights reserved.
//

import UIKit

class InformationTableViewCell: UITableViewCell {
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        //Labels used...
        var Label1 = UILabel(frame: CGRectMake(80/3, 50/3, 100, 30))
        Label1.text = ""
        Label1.tag = 15
        contentView.addSubview(Label1)
    }
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
